#include "solver.h"
#include "evaluation.h"

Solver::Solver(Params* params){
	this->params = params;
}

GDSolver::GDSolver(Params* params) : Solver(params){
}

T GDSolver::learn(Data* data, Network* nn){	
	T learning_rate = params->get("learning_rate");
	T target_loss = params->get("target_loss");
	const int max_iter = (int)params->get("max_iter");
	vector<T> loss_history(max_iter);

	T loss = 1e10;
	T best_loss = 1e10;
	//Network n2;
	Tensor dXin;
	int iter = 0;
	while (loss>target_loss && iter<max_iter){
		//Timer t; t.tic("solver step");
		nn->forward(data->X, data->y, loss);
		nn->backward(dXin, data->y, loss);
		for (Layer* l : nn->layers) if (l->W.rows)
			update(l->W, l->dW);
		//t.toc();
		loss_history[iter] = loss;		
		if (iter && loss > loss_history[iter-1]){
			//params->set("learning_rate", params->get("learning_rate")*0.5);
			//printf("learning rate: %g\n", params->get("learning_rate"));
		}
		printf("%3d %f\n", iter++, loss);		
		if (params->get("learning_rate") < 1e-20)
			break;
		//if (loss < best_loss){
		//	best_loss = loss;
		//	n2 = *nn;
		//}
	}
	return loss;
}

#undef min
#undef max
void GDSolver::update(Mat_<T>& X, Mat_<T>& dX){
	T learning_rate = params->get("learning_rate");
	T lambda = params->get("regularization_factor");
	X = X - learning_rate*(dX + lambda*X);
	if (maxall(X) > 1e3){
		printf("large values in weight matrix\n");
		X = X / maxall(X);
	}
}

SGDSolver::SGDSolver(Params* params) : Solver(params){
}

T SGDSolver::learn(Data* data, Network* nn){
	T learning_rate = params->get("learning_rate");
	const int max_epochs = (int) params->get("max_epochs");
	const int batch_size = (int) params->get("batch_size");
	const int learning_rate_epochs = (int) params->get("learning_rate_epochs");
	const int eval_epochs = (int)params->get("eval_epochs");

	loss_history.clear();

	T loss = 1e10;
	//Network n2;
	Tensor dXin;
	int iter = 0;
	int epoch = 0;
	data->createBatches(batch_size);
	while (epoch<max_epochs){
		//one learning step: forward, backward and update
		//Timer t; t.tic("solver step");
		Data batch = data->nextBatch();
		nn->forward(batch.X, batch.y, loss);
		nn->backward(dXin, batch.y, loss);
		for (Layer* l : nn->layers) if (l->W.rows)
			update(l->W, l->dW);
		//t.toc();

		//log loss
		loss_history.push_back(loss);
		printf("epoch:%3d iter:%4d %f\n", epoch, iter, loss);
		iter++;
		if (iter*batch_size / data->getNoInstances() > epoch){
			epoch++;

			if (epoch%eval_epochs == 0){
				//((Dropout*)nn->layers[8])->testMode();
				Evaluation eval;
				printf("training ");
				eval.classification_error(data, nn);
				data->createBatches(batch_size);
				//((Dropout*)nn->layers[8])->trainingMode();
			}

			if (epoch%learning_rate_epochs == 0){
				params->set("learning_rate", params->get("learning_rate")*params->get("learning_rate_decay"));
				printf("learning rate changed to: %g\n", params->get("learning_rate"));
			}
		}
	}
	return loss;
}

void SGDSolver::update(Mat_<T>& X, Mat_<T>& dX){
	T learning_rate = params->get("learning_rate");
	T lambda = params->get("regularization_factor");
	X = X - learning_rate*(dX + lambda*X);
}
