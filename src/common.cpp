#include "common.h"

FileGetter::FileGetter(char* folderin, char* ext){
	strcpy(folder, folderin);
	char folderstar[256];
	if (!ext){
		sprintf(folderstar, "%s\\*.*", folder);
	}
	else{
		sprintf(folderstar, "%s\\*.%s", folder, ext);
	}	
	hfind = FindFirstFileA(folderstar, &found);
	chk = FindNextFileA(hfind, &found);
	while (chk && (strcmp(found.cFileName, ".") == 0 || strcmp(found.cFileName, "..") == 0))
		chk = FindNextFileA(hfind, &found);
}

int FileGetter::getNextFile(char* fname){	
	int ret = 0;
	if (chk){
		ret = chk;
		strcpy(fname, found.cFileName);
		chk = FindNextFileA(hfind, &found);
	}
	return ret;
}

int FileGetter::getNextAbsFile(char* fname){
	int ret = 0;
	if (chk){
		ret = chk;
		sprintf(fname, "%s\\%s", folder, found.cFileName);
		chk = FindNextFileA(hfind, &found);
	}
	return ret;
}

Timer::Timer(){
	strcpy(str, "");
	startTime = clock();
};
void Timer::tic(){
	startTime = clock();
}
void Timer::tic(char* str2){
	strcpy(str, str2);
	startTime = clock();
}
double Timer::toc(int nrtimes){
	double elapsed = (clock() - startTime) / CLOCKS_PER_SEC;
	elapsed /= nrtimes;
	printf("%s Time elapsed ", str);
	int remaining = elapsed;
	int hours = 0, minutes = 0;
	if (remaining >= 3600){
		hours = remaining / 3600;
		remaining = remaining % 3600;
		printf("%d hours, ", hours);
	}
	if (remaining >= 60)
	{
		minutes = remaining / 60;
		remaining = remaining % 60;
		printf("%d minutes, ", minutes);
	}
	printf("%.3f seconds\n", elapsed - hours*3600 - minutes*60);
	return elapsed;
}

void Timer::getTime(char* ttime){
	char days[7][20] = {
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	};
	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	sprintf(ttime, "%04d.%02d.%02d %s %02d:%02d:%02d", 1900 + timeinfo->tm_year, timeinfo->tm_mon + 1, timeinfo->tm_mday,
		days[timeinfo->tm_wday], timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
}

T maxall(Mat_<T> mat){
	T res = abs(mat(0, 0));
	for (int i = 0; i < mat.rows; i++) for (int j = 0; j < mat.cols; j++)
		res = max(res, abs(mat(i, j)));
	return res;
}

int argmax_row(Mat_<T> mat, int row){
	int res = 0;
	for (int i = 1; i<mat.cols; i++){
		if (mat(row, i) > mat(row, res))
			res = i;
	}
	return res;
}

bool isInside(int row, int col, Mat_<T> img){
	return (row >= 0 && col >= 0 && row < img.rows && col < img.cols);
}