#pragma once
#include "network.h"
#include "common.h"
#include "data.h"
#include "params.h"

class Solver{	
protected:
	vector<T> loss_history;
public:
	Params* params;
	Solver(Params* params);
	void update(Mat_<T>& X, Mat_<T>& dX);
	void learn(Data* data, Network* nn);
};

class GDSolver : public Solver{
public:
	GDSolver(Params* params);
	void update(Mat_<T>& X, Mat_<T>& dX);
	T learn(Data* data, Network* nn);
};

class SGDSolver : public Solver{
public:
	SGDSolver(Params* params);
	void update(Mat_<T>& X, Mat_<T>& dX);
	T learn(Data* data, Network* nn);
};