#pragma once
#include "common.h"

class Params{
	map<string, T> p;
public:
	Params();
	void set(string name, T val);
	T get(string name);
	void saveToFile(FILE* f);
};