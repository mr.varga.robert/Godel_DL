#pragma once
#include "common.h"
#include "tensor.h"

class Data{	
	void split(Mat img, Tensor& X, int row);
	int no_classes;
	int batch_size, batch_poz;
	vector<int> batch_v;
public:	
	Tensor X;
	Mat_<T> y;
	Data();
	void loadImageDataset(char* folder);
	void loadFromFile(char* fname);
	void normalizeData();
	int getNoInstances(){ return y.rows; }
	int getNoClasses(){ return no_classes; }
	int getDim(){ return X.getDim(); }		
	void createBatches(int batch_size, int perform_shuffle = 1);
	Data nextBatch();
};