#include "tensor.h"

Tensor::Tensor(){
	currentForm = TENSOR_FORM;
}

void Tensor::create(int noInst, int depth, int height, int width){
	this->noInst = noInst;
	this->depth = depth;
	this->height = height;
	this->width = width;
	currentForm = TENSOR_FORM;

	if (tform.size() == noInst && tform[0].size() == depth && tform[0][0].rows == height && tform[0][0].cols == width)
		return;
	tform.clear();
	tform.resize(noInst);
	for (int i = 0; i < noInst; i++){
		vector<Mat_<T>> v;
		v.resize(depth);
		for (int j = 0; j < depth; j++){
			Mat_<T> img(height, width);
			v[j] = img;
		}
		tform[i] = v;
	}
}

void Tensor::create(int noInst, int depth){
	this->noInst = noInst;
	this->depth = depth;
	this->height = 1;
	this->width = 1;
	mform.create(noInst, depth);
	currentForm = MATRIX_FORM;
}

Tensor::Tensor(int noInst, int depth, int height, int width){
	create(noInst, depth, height, width);
}

Tensor::Tensor(int noInst, int depth){	
	create(noInst, depth, 1, 1);
}

void Tensor::createLike(Tensor& o){
	currentForm = o.getCurrentForm();
	if (currentForm == MATRIX_FORM)
		create(o.getNoInstances(), o.getDepth());
	else
		create(o.getNoInstances(), o.getDepth(), o.getHeight(), o.getWidth());
}

void Tensor::copyDims(Tensor& o){
	this->noInst = o.noInst;
	this->depth = o.depth;
	this->height = o.height;
	this->width = o.width;
}

Tensor::Tensor(const Tensor& o){
	tform.clear();
	tform.resize(o.tform.size());
	for (int i = 0; i < tform.size(); i++){
		tform[i].resize(o.tform[i].size());
		for (int j = 0; j < tform[i].size(); j++){
			tform[i][j] = o.tform[i][j].clone();
		}
	}
	mform = o.mform.clone();
	currentForm = o.currentForm;
	noInst = o.noInst;
	depth = o.depth;
	height = o.height;
	width = o.width;
}

Tensor& Tensor::operator = (Tensor& o){
	tform.clear();
	tform.resize(o.tform.size());
	for (int i = 0; i < tform.size(); i++){
		tform[i].resize(o.tform[i].size()); //drmem
		for (int j = 0; j < tform[i].size(); j++){
			tform[i][j] = o.tform[i][j].clone();
		}
	}
	mform = o.mform.clone();
	currentForm = o.currentForm;
	noInst = o.noInst;
	depth = o.depth;
	height = o.height;
	width = o.width;
	return *this;
}

Tensor::Tensor(Tensor&& o){
	printf("move ctr");
}

Tensor Tensor::copy(){
	Tensor t;
	t.tform.resize(tform.size());
	for (int i = 0; i < tform.size(); i++){
		t.tform[i].resize(tform[i].size());
		for (int j = 0; j < tform[i].size(); j++){
			t.tform[i][j] = tform[i][j].clone();
		}
	}
	t.mform = mform.clone();
	t.currentForm = currentForm;
	t.noInst = noInst;
	t.depth = depth;
	t.height = height;
	t.width = width;
	return t;
}

int Tensor::getNoInstances() const{
	return noInst;
}

int Tensor::getDepth() const{
	return depth;
}

int Tensor::getHeight() const{
	return height;
}

int Tensor::getWidth() const{
	return width;
}

int Tensor::getDim() const{
	return height*width*depth;
}

Mat_<T>& Tensor::linearize(){	
	if (currentForm == MATRIX_FORM)
		return mform;
	int w = getWidth();
	int h = getHeight();
	mform.create(getNoInstances(), getDim());
	for (int i = 0; i < tform.size(); i++){
		for (int j = 0; j < tform[i].size(); j++){
			for (int row = 0; row < tform[i][j].rows; row++){
				for (int col = 0; col < tform[i][j].cols; col++){
					mform(i, col + w*(row + h*j)) = tform[i][j](row, col);
				}
			}
		}
	}	
	tform.clear();
	currentForm = MATRIX_FORM;
	return mform;
}

void Tensor::tensorize(){
	if (currentForm == TENSOR_FORM)
		return;
	tform.resize(noInst);
	for (int i = 0; i < noInst; i++){
		tform[i].resize(depth);
		for (int j = 0; j < depth; j++){
			tform[i][j].create(height, width);
			for (int row = 0; row < height; row++){
				for (int col = 0; col < width; col++){
					tform[i][j](row, col) = mform(i, col + width*(row + height*j));
				}
			}
		}
	}
	mform.release();
	currentForm = TENSOR_FORM;
}