#include "params.h"

Params::Params(){
	//set default parameter values
	p["regularization_factor"] = 1e-20;
	p["eval_epochs"] = 10;
}

void Params::set(string name, T val){
	p[name] = val;
}

T Params::get(string name){
	if (p.find(name) == p.end()){
		char buff[256]{};
		sprintf(buff, "Params::get - Inexistent parameter: %s", name.c_str());
		throw exception(buff);
	}
	else
		return p[name];
}

void Params::saveToFile(FILE* f){
	for (auto it : p){
		fprintf(f, "%30s : %4.5g\n", it.first.c_str(), it.second);
	}
}
